---
image: '/images/landing.avif'
imageAlt: 'Dominik jumping from a tree to a wooden playground structure'
---
Hi! Welcome to my website. This site contains some (probably outdated) [blog posts](/posts/), recordings and slides of talks I've held.
