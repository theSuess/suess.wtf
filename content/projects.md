---
title: "projects"
type: "misc"
layout: "projects"
projects:
- name: SSI Validation tool
  description: Generate verification code for arbitrary dive centers so you don't have to talk to people
  url: https://ssi.suess.wtf
- name: distrust
  description: Bridge [DiscourseConnect](https://meta.discourse.org/t/setup-discourseconnect-official-single-sign-on-for-discourse-sso/13045) with OIDC! Use your Discourse forum as your IDM
  url: https://github.com/parkour-vienna/distrust
---
