---
title: "publications"
layout: publications
type: misc
publications:
- name: 'Observing Lambdas using the OpenTelemetry Collector Extension Layer'
  url: 'https://opentelemetry.io/blog/2025/observing-lambdas/'
  date: '2025-02-05'
  type: 'blog post on the OpenTelemetry.io blog'
- name: 'License to Observe: Why Observability Solutions Need Agents'
  url: 'https://www.usenix.org/publications/loginonline/license-observe'
  date: '2025-02-24'
  type: 'article in the ;login: online publication'
- name: Recording and replay of block device operations for file system consistency analysis
  url: '/publications/2022_sitelen.pdf'
  date: 2022-03-09
  type: 'bachelor thesis, pdf'
- name: 'Call me, maybe: designing an incident response process'
  url: 'https://grafana.com/blog/2024/03/28/call-me-maybe-designing-an-incident-response-process/'
  date: 2024-03-28
  type: 'blog post on the Grafana blog'
- name: 'How to fix performance issues using k6 and the Grafana LGTM Stack'
  url: 'https://grafana.com/blog/2023/07/05/how-to-fix-performance-issues-using-k6-and-the-grafana-lgtm-stack/'
  date: 2023-07-05
  type: 'blog post on the Grafana blog'
- name: 'Moving GitLab to Azure'
  url: 'https://engineering.cloudflight.io/moving-gitlab-to-azure'
  date: 2023-04-24
  type: 'blog post on the Cloudflight engineering blog'
---

Things I wrote somewhere else or that don't fit the blog format.
