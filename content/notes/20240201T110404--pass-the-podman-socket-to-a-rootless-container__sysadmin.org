#+title:      Pass the podman socket to a rootless container
#+date:       [2024-02-01 Thu 11:04]
#+filetags:   :sysadmin:
#+identifier: 20240201T110404
#+slug: 20240201T110404

The issue preventing this from /just working/ is caused by selinux.

To disable this, either use the ~--security-opt label=disable~ or specify the following in compose files:
#+begin_src yaml
  runner:
    image: drone/drone-runner-docker:1
    security_opt:
      - label:disable
#+end_src

Source: [[denote:20240610T081524][Sysadmin: How to use Podman inside of a container]]


