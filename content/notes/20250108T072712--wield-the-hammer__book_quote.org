#+title:      Wield the Hammer
#+date:       [2025-01-08 Wed 07:27]
#+filetags:   :book:quote:
#+identifier: 20250108T072712
#+slug:       20250108T072712

#+begin_quote
The Maric workers' saying translates as 'the most effective way to wield the hammer is to stop using it'
#+end_quote
From [[denote:20250108T071746][City of Last Chances]].
