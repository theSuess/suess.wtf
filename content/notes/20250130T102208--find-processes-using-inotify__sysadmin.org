#+title:      Find processes using inotify
#+date:       [2025-01-30 Thu 10:22]
#+filetags:   :sysadmin:
#+identifier: 20250130T102208
#+slug:       20250130T102208

File watcher limits can be annoying and most online resources just tell you to increase the system limits. I'd rather fix the issue at the root and find out which processes are using the ~inotify~ api.

Thanks to [[https://stackoverflow.com/users/3945377/pauli-nieminen][Pauli Nieminen]], in [[https://stackoverflow.com/questions/25470672/how-to-stop-inotify-from-monitoring-a-directory][this StackOverflow thread]] for the following snippet:

#+begin_src bash
ps -p $(find /proc/*/fd/* -type l -lname 'anon_inode:inotify' -print 2> /dev/null | sed -e 's/^\/proc\/\([0-9]*\)\/.*/\1/')
#+end_src
