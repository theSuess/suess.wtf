---
date: 2024-09-28
title: 'License to Observe: Why observability solutions need agents'
slides:
  - variant: 'DevOpsDays Ljubljana 2024'
    slidesURL: '/slides/2024-09-28_license_to_observe.pdf'
  - variant: 'KCD Austria 2024'
    slidesURL: '/slides/2024-10-09_license_to_observe.pdf'
    recordingURL: 'https://www.youtube.com/watch?v=7rSwzGSosVE'
categories:
- talks
---

{{< youtube 7rSwzGSosVE >}}
