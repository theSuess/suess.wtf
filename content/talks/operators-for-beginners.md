---
date: 2020-01-24
title: 'Operators for Beginners'
slides:
  - variant: 'DevConf.cz 2020'
    slidesURL: '/slides/2020-01-24_operators_for_beginners.pdf'
    recordingURL: 'https://www.youtube.com/watch?v=0p2m8TzBK-k'
categories:
- talks
---

{{< youtube 0p2m8TzBK-k >}}
