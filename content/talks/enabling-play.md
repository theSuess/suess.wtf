---
date: 2024-06-14
title: "Enabling Play - Leveling up your DevEx Game"
slides:
  - variant: 'DevConf.cz 2024'
    slidesURL: '/slides/2024-06-14_enabling_play.pdf'
    recordingURL: 'https://www.youtube.com/watch?v=lPO2Os8t298'
categories:
- talks
---

{{< youtube lPO2Os8t298 >}}
